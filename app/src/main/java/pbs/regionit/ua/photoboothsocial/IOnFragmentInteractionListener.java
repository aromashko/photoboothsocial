package pbs.regionit.ua.photoboothsocial;


import android.support.v4.app.Fragment;

/**
* Created by vuzya on 01.06.2014.
*/
public interface IOnFragmentInteractionListener {
    public void navigateToFragment(Fragment fragment, boolean addToBackstack);
}
