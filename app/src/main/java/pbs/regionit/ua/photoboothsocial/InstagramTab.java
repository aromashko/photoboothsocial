package pbs.regionit.ua.photoboothsocial;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;

import pbs.regionit.ua.photoboothsocial.utils.Const;

/**
 * Created by user on 06.02.2015.
 */
public class InstagramTab extends Fragment implements CompoundButton.OnCheckedChangeListener {
    Switch swch_activate, swch_activate_print, swch_activate_print_fix_hashtag,
            swch_limit_photo_print;
    Button choose_photo_template;
    EditText edt_share_text, edt_fixed_hashtag, edt_count_of_photo, edt_count_photo_print,
            edt_period_photo_print;
    ImageView img_template;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.instagram_tab, container, false);
        initView(v);

        return v;
    }

    public void initView(View v){
        swch_activate = (Switch) v.findViewById(R.id.swch_activate);
        swch_activate_print = (Switch) v.findViewById(R.id.swch_activate_print);
        swch_activate_print_fix_hashtag = (Switch) v.findViewById(R.id.swch_activate_print_fix_hashtag);
        swch_limit_photo_print = (Switch) v.findViewById(R.id.swch_limit_photo_print);
        choose_photo_template= (Button) v.findViewById(R.id.choose_photo_template);
        edt_share_text= (EditText) v.findViewById(R.id.edt_share_text);
        edt_fixed_hashtag= (EditText) v.findViewById(R.id.edt_fixed_hashtag);
        edt_count_of_photo = (EditText) v.findViewById(R.id.edt_count_of_photo);
        edt_count_photo_print = (EditText) v.findViewById(R.id.edt_count_photo_print);
        edt_period_photo_print= (EditText) v.findViewById(R.id.edt_period_photo_print);
        img_template= (ImageView) v.findViewById(R.id.img_template);

        swch_activate.setChecked(getActivatedPrefs());
        swch_activate.setOnCheckedChangeListener(InstagramTab.this);
        swch_activate_print.setChecked(getInstagramPrintActivatedPrefs());
        swch_activate_print.setOnCheckedChangeListener(InstagramTab.this);
        swch_activate_print_fix_hashtag.setChecked(getInstagramPrintFixedTagActivatedPrefs());
        swch_activate_print_fix_hashtag.setOnCheckedChangeListener(InstagramTab.this);
        swch_limit_photo_print.setChecked(getLimitPhotoPrintPrefs());
        swch_limit_photo_print.setOnCheckedChangeListener(InstagramTab.this);

        edt_share_text.setText(getShareTextPrefs());
        edt_share_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                storePrefs(Const.INSTAGRAM_SHARE_TEXT, s.toString());
            }
        });
        edt_fixed_hashtag.setText(getFixedHashTagPrefs());
        edt_fixed_hashtag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                storePrefs(Const.INSTAGRAM_FIXED_HASHTAG, s.toString());
            }
        });
        edt_count_of_photo.setText(getCountOfPhotoToViewPrefs());
        edt_count_of_photo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                storePrefs(Const.INSTAGRAM_COUNT_OF_PHOTO_TO_VIEW, s.toString());
            }
        });
        edt_count_photo_print.setText(getMaxPrintPhotoPrefs());
        edt_count_photo_print.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                storePrefs(Const.INSTAGRAM_MAX_COUNT_PHOTO_TO_PRINT, s.toString());
            }
        });
        edt_period_photo_print.setText(getPeriodToPrintPrefs());
        edt_period_photo_print.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                storePrefs(Const.INSTAGRAM_PERIOD_TO_PRINT, s.toString());
            }
        });


    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.swch_activate:
                storePrefs(Const.INSTAGRAM_ACTIVATED, isChecked);
                break;
            case R.id.swch_activate_print:
                storePrefs(Const.INSTAGRAM_ACTIVATED, isChecked);
                break;
            case R.id.swch_activate_print_fix_hashtag:
                storePrefs(Const.INSTAGRAM_ACTIVATED, isChecked);
                break;
            case R.id.swch_limit_photo_print:
                storePrefs(Const.INSTAGRAM_ACTIVATED, isChecked);
                break;
        }
    }

    public void storePrefs(String key,boolean value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void storePrefs(String key, String value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public boolean getActivatedPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.INSTAGRAM_ACTIVATED, false);
        return autoShare;
    }

    public boolean getInstagramPrintActivatedPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.INSTAGRAM_PRINT_ACTIVATED, false);
        return autoShare;
    }

    public boolean getInstagramPrintFixedTagActivatedPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.INSTAGRAM_PRINT_FIXED_TAG_ACTIVATED, false);
        return autoShare;
    }


    public boolean getLimitPhotoPrintPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.INSTAGRAM_LIMIT_PHOTO_PRINT, false);
        return autoShare;
    }

    public String getShareTextPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String albumName = prefs.getString(Const.INSTAGRAM_SHARE_TEXT, "");
        return albumName;
    }

    public String getFixedHashTagPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String albumName = prefs.getString(Const.INSTAGRAM_FIXED_HASHTAG, "");
        return albumName;
    }

    public String getCountOfPhotoToViewPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String albumName = prefs.getString(Const.INSTAGRAM_COUNT_OF_PHOTO_TO_VIEW, "");
        return albumName;
    }

    public String getMaxPrintPhotoPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String albumName = prefs.getString(Const.INSTAGRAM_MAX_COUNT_PHOTO_TO_PRINT, "");
        return albumName;
    }

    public String getPeriodToPrintPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String albumName = prefs.getString(Const.INSTAGRAM_PERIOD_TO_PRINT, "");
        return albumName;
    }
}


