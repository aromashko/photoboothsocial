package pbs.regionit.ua.photoboothsocial;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.vk.sdk.util.VKUtil;

import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import pbs.regionit.ua.photoboothsocial.db.DB;
import pbs.regionit.ua.photoboothsocial.fragments.PhotoFullFragment;
import pbs.regionit.ua.photoboothsocial.fragments.PhotoPreviewFragment;
import pbs.regionit.ua.photoboothsocial.utils.Const;
import pbs.regionit.ua.photoboothsocial.utils.GcmUtils;


public class MainActivity extends FragmentActivity implements  IOnFragmentInteractionListener {
    String TAG = MainActivity.class.getSimpleName();

    String backgroundPath;
    AsyncHttpClient client;
    FrameLayout frameMain;
    public final static String BROADCAST_ACTION = "pbs.regionit.ua.photoboothsocial.refresh_list";
    public final static String BROADCAST_ACTION_LOG_OUT = "pbs.regionit.ua.photoboothsocial.logout";
    protected FragmentManager fragmentManager;
    int sdk;
    Button btn_logout;
    BroadcastReceiver receiver_refresh, receiver_logout;
    DB db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        db = new DB(this);
        db.open();
        initView();
        getSocialHashKey();
        client = new AsyncHttpClient();
        checkRegistration();

        Fragment photoPreviewfragment = PhotoPreviewFragment.newInstance();
        navigateToFragment(photoPreviewfragment, false);

        receiver_refresh = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameMain);
                if (fragment instanceof PhotoPreviewFragment){
                    ((PhotoPreviewFragment) fragment).refreshList();
                }
            }
        };
        // создаем фильтр для BroadcastReceiver
        IntentFilter intFiltRefresh = new IntentFilter(MainActivity.BROADCAST_ACTION);
        // регистрируем (включаем) BroadcastReceiver
       registerReceiver(receiver_refresh, intFiltRefresh);

        receiver_logout = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameMain);
                if (fragment instanceof PhotoFullFragment){
                    ((PhotoFullFragment) fragment).logoutSocialNetwork();
                    ((PhotoFullFragment) fragment).clearCache();
                }
            }
        };
        // создаем фильтр для BroadcastReceiver
        IntentFilter intFiltLogOut = new IntentFilter(MainActivity.BROADCAST_ACTION_LOG_OUT);
        // регистрируем (включаем) BroadcastReceiver
        registerReceiver(receiver_logout, intFiltLogOut);


        //exportDatabase("photoboothsocial");
    }

    public DB getDb() {
        return db;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        db.close();
        unregisterReceiver(receiver_logout);
        unregisterReceiver(receiver_refresh);
    }


    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        backgroundPath = getBackgroundPath();
        if (!backgroundPath.isEmpty()){
            Bitmap bitmap = BitmapFactory.decodeFile(backgroundPath);
            Drawable d = new BitmapDrawable(getResources(),bitmap);
            sdk = Build.VERSION.SDK_INT;
            if (sdk < 16){
                frameMain.setBackgroundDrawable(d);
            }else {
                frameMain.setBackground(d);
            }
        }
        else{
            frameMain.setBackgroundResource(R.drawable.rsz_bg_standart);
        }
    }

    @Override
    public void onBackPressed() {
        if (fragmentManager.getBackStackEntryCount() == 0) {
            //don't allow finish app with back button
            //this.finish();
        } else {
            fragmentManager.popBackStack();
        }
    }

    public void getSocialHashKey(){
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
        StringBuilder builder = new StringBuilder();
        for(String s : fingerprints) {
            builder.append(s);
        }
        Log.d("KeyHash VK:", builder.toString());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(Const.SOCIAL_NETWORK_TAG);
        if (fragment != null ) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }



    public void initView(){
        frameMain = (FrameLayout)findViewById(R.id.frameMain);
    }

   public void storePrefs(String key,boolean value) {
        final SharedPreferences prefs = getSharedPreferences(Const.PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public String getBackgroundPath() {
        final SharedPreferences prefs =  getSharedPreferences(Const.PREFS, MODE_PRIVATE);
        String backgroundPath = prefs.getString(Const.SAVED_BACKGROUND, "");
        return backgroundPath;
    }

    public boolean isConnected() {
        final SharedPreferences prefs =  getSharedPreferences(Const.PREFS, MODE_PRIVATE);
        boolean isConnected = prefs.getBoolean(Const.CONNECTED_TO_TERMINAL, false);
        return isConnected;
    }

    public void checkRegistration(){
        JSONObject jsonObject=null;
        ByteArrayEntity entity = null;
        if (getRegid().isEmpty() || !isConnected()){
            Toast toast =Toast.makeText(this, R.string.didnt_connect_to_terminal, Toast.LENGTH_LONG);
            LinearLayout toastLayout = (LinearLayout) toast.getView();
            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(30);
            toast.show();
        }
        else {
            try {
                jsonObject = new JSONObject();
                jsonObject.put("registrationid", getRegid());
                JSONArray jsonArray = new JSONArray();
                JSONObject queryObj = new JSONObject();
                queryObj.put("type", "check_registration");
                jsonArray.put(queryObj);
                jsonObject.put("query", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("jsonObject", jsonObject.toString());
            try {
                entity = new ByteArrayEntity(jsonObject.toString().getBytes("UTF-8"));
            }
            catch(UnsupportedEncodingException e){
                e.printStackTrace();

            }

            client.post(this, getResources().getString(R.string.api_url), entity, "application/json",  new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(JSONObject response) {
                    Log.d(TAG, String.valueOf(response));
                    try {
                        JSONArray array = response.getJSONArray("response");
                        String status = array.getJSONObject(0).getString("status");
                        String type = array.getJSONObject(0).getString("type");
                        Log.d(TAG, status);
                        Log.d(TAG, type);

                        if (status.equals("true") && type.equals("check_registration")){
                            storePrefs(Const.CONNECTED_TO_TERMINAL, true);
                        }
                        else{
                            Toast toast =Toast.makeText(MainActivity.this, R.string.didnt_connect_to_terminal, Toast.LENGTH_LONG);
                            LinearLayout toastLayout = (LinearLayout) toast.getView();
                            TextView toastTV = (TextView) toastLayout.getChildAt(0);
                            toastTV.setTextSize(30);
                            toast.show();

                        }

                    } catch (JSONException e) {
                        Log.w(TAG, "Parsing error", e);
                    }
                }

                @Override
                public void onFailure(Throwable e, JSONObject errorResponse) {
                    Toast toast =Toast.makeText(MainActivity.this, R.string.didnt_connect_or_no_internet, Toast.LENGTH_LONG);
                    LinearLayout toastLayout = (LinearLayout) toast.getView();
                    TextView toastTV = (TextView) toastLayout.getChildAt(0);
                    toastTV.setTextSize(30);
                    toast.show();
                    super.onFailure(e, errorResponse);
                }
            });
        }
    }

    public String getRegid() {
        String regid = GcmUtils.getRegistrationId(MainActivity.this);
        return regid;
    }

    @Override
    public void navigateToFragment(Fragment fragment, boolean addToBackstack) {

        FragmentTransaction ft = fragmentManager.beginTransaction()
                .replace(R.id.frameMain, fragment);
        if(addToBackstack){
            ft.addToBackStack(null);
        }
        ft.commit();
    }


    public void navigateToFragment(android.app.Fragment fragment, boolean addToBackstack) {
        android.app.FragmentManager fragmentManager1 = getFragmentManager();
        android.app.FragmentTransaction ft = fragmentManager1.beginTransaction()
                .replace(R.id.frameMain, fragment);
        if(addToBackstack){
            ft.addToBackStack(null);
        }
        ft.commit();
    }

    public void exportDatabase(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//"+getPackageName()+"//databases//"+databaseName+"";
                String backupDBPath = "photobooth_backup.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}