package pbs.regionit.ua.photoboothsocial;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.net.URISyntaxException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

import pbs.regionit.ua.photoboothsocial.utils.Const;
import pbs.regionit.ua.photoboothsocial.utils.FileUtils;

/**
 * Created by user on 28.01.2015.
 */
public class SelfieTab extends Fragment implements CompoundButton.OnCheckedChangeListener{
    Switch swch_activate, show_selfie_in_photobooth;
    Button choose_photo_template;
    private static final int REQUEST_CODE_JSON =1;
    public static final String TAG=SelfieTab.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.selfie_tab, container, false);
        initView(v);

        return v;
    }

    public void initView(View v){
        swch_activate= (Switch) v.findViewById(R.id.swch_activate);
        show_selfie_in_photobooth= (Switch) v.findViewById(R.id.show_selfie_in_photobooth);
        choose_photo_template= (Button) v.findViewById(R.id.choose_photo_template);

        swch_activate.setChecked(getActivatedPrefs());
        swch_activate.setOnCheckedChangeListener(SelfieTab.this);
        show_selfie_in_photobooth.setChecked(getShowSelfieInPhotoboothPrefs());
        show_selfie_in_photobooth.setOnCheckedChangeListener(SelfieTab.this);

        choose_photo_template.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFile(REQUEST_CODE_JSON);
            }
        });
    }

    private void pickFile(int requestCode){
        Intent intent = new Intent();
        intent.putExtra("CONTENT_TYPE", "file/json");
        intent.setType("file/json");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, requestCode);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_JSON:
                if (resultCode == getActivity().RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    Log.d(TAG, "File Uri: " + uri.toString());
                    // Get the path
                    String path = null;
                    try {
                        path = FileUtils.getPath(getActivity(), uri);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    if (path.contains(".json")){
                        openFile(path);
                    }
                    else{
                        Toast toast =Toast.makeText(getActivity(), "Неправильный формат файла", Toast.LENGTH_LONG);
                        LinearLayout toastLayout = (LinearLayout) toast.getView();
                        TextView toastTV = (TextView) toastLayout.getChildAt(0);
                        toastTV.setTextSize(30);
                        toast.show();
                    }
                    Log.d(TAG, "File Path: " + path);
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void openFile(String path){
        try {
            File yourFile = new File(path);
            FileInputStream stream = new FileInputStream(yourFile);
            String jString = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                /* Instead of using default, pass in a decoder. */
                jString = Charset.defaultCharset().decode(bb).toString();
            }
            finally {
                stream.close();
            }


            JSONObject jObject = new JSONObject(jString);
            Log.e(TAG, "json"+ jObject.toString());



        } catch (Exception e) {e.printStackTrace();}
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.swch_activate:
                storePrefs(Const.SELFIE_ACTIVATED, isChecked);
                break;
            case R.id.show_selfie_in_photobooth:
                storePrefs(Const.SHOW_SELFIE_IN_PHOTOBOOTH_ACTIVATED, isChecked);
                break;
        }
    }

    public void storePrefs(String key,boolean value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getActivatedPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.SELFIE_ACTIVATED, false);
        return autoShare;
    }

    public boolean getShowSelfieInPhotoboothPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.SHOW_SELFIE_IN_PHOTOBOOTH_ACTIVATED, false);
        return autoShare;
    }
}
