package pbs.regionit.ua.photoboothsocial;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import pbs.regionit.ua.photoboothsocial.utils.Const;

/**
 * Created by user on 28.01.2015.
 */
public class SessionTab extends Fragment implements CompoundButton.OnCheckedChangeListener{
    Switch swch_activate;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.session_tab, container, false);
        initView(v);

        return v;
    }

    public void initView(View v){
        swch_activate= (Switch) v.findViewById(R.id.swch_activate);
        swch_activate.setChecked(getActivatedPrefs());
        swch_activate.setOnCheckedChangeListener(SessionTab.this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.swch_activate:
                storePrefs(Const.SESSION_ACTIVATED, isChecked);
                break;
        }
    }

    public void storePrefs(String key,boolean value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getActivatedPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.SESSION_ACTIVATED, false);
        return autoShare;
    }
}
