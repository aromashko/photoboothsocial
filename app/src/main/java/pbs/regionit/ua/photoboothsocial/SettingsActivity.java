package pbs.regionit.ua.photoboothsocial;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.widget.Toast;

import com.facebook.UiLifecycleHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import pbs.regionit.ua.photoboothsocial.db.DB;
import pbs.regionit.ua.photoboothsocial.utils.Const;
import pbs.regionit.ua.photoboothsocial.utils.GcmUtils;

/**
 * Created by RmIIIK on 17.11.2014.
 */
public class SettingsActivity extends FragmentActivity {
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    String TAG = SettingsActivity.class.getSimpleName();
    GoogleCloudMessaging gcm;
    public static final String SENDER_ID  = "100792855533";
    String regid;
    ProgressDialog pd;
    private UiLifecycleHelper uiHelper;
    DB db;
    /** Called when the activity is first created. */
    private FragmentTabHost tabHost;



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
        db.close();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(SettingsActivity.this, null);
        uiHelper.onCreate(savedInstanceState);
        // Check device for Play Services APK.
        // If this check succeeds, proceed with normal processing.
        // Otherwise, prompt user to get valid Play Services APK.
        if (checkPlayServices()) {
            getRegid();
            if (regid.isEmpty()) {
                registerInBackground();
            }

            setContentView(R.layout.fragment_settings);

            tabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);
            tabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

            tabHost.addTab(tabHost.newTabSpec("VK").setIndicator("Vkontakte"),
                    vkTab.class, null);
            tabHost.addTab(tabHost.newTabSpec("FB").setIndicator("Facebook"),
                    fbTab.class, null);
            tabHost.addTab(tabHost.newTabSpec("Instagram").setIndicator("Instagram"),
                    InstagramTab.class, null);
            tabHost.addTab(tabHost.newTabSpec("SMS").setIndicator("SMS"),
                    smsTab.class, null);
            tabHost.addTab(tabHost.newTabSpec("Mail").setIndicator("Mail"),
                    mailTab.class, null);
            tabHost.addTab(tabHost.newTabSpec("Print").setIndicator("Печать"),
                    PrintTab.class, null);
            tabHost.addTab(tabHost.newTabSpec("Selfie").setIndicator("Селфи"),
                    SelfieTab.class, null);
            tabHost.addTab(tabHost.newTabSpec("Session").setIndicator("Сессии"),
                    SessionTab.class, null);
            tabHost.addTab(tabHost.newTabSpec("General").setIndicator("Общие"),
                    generalTab.class, null);

            db = new DB(this);
            db.open();


        }
        else{
            Toast.makeText(SettingsActivity.this, "No valid Google Play Services APK found.", Toast.LENGTH_LONG).show();
            return;
        }

    }


    public DB getDb() {
        return db;
    }

    public void showProgress(String message) {
        pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage(message);
        pd.setCancelable(true);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    public void hideProgress() {
        if (pd != null) {
            pd.dismiss();
        }
    }

    public String getRegid() {
        regid = GcmUtils.getRegistrationId(SettingsActivity.this);
        return regid;
    }


    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("OnActivityResult", "Settings");

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(Const.SOCIAL_NETWORK_TAG);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();
        checkPlayServices();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new RegisterGCMTask().execute(null, null, null);

    }


    class RegisterGCMTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String msg = "";
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(SettingsActivity.this);
                }
                regid = gcm.register(SENDER_ID);
                msg = "Device registered, registration ID=" + regid;

                // You should send the registration ID to your server over HTTP,
                // so it can use GCM/HTTP or CCS to send messages to your app.
                // The request to your server should be authenticated if your app
                // is using accounts.
                //sendRegistrationIdToBackend();

                // For this demo: we don't need to send it because the device
                // will send upstream messages to a server that echo back the
                // message using the 'from' address in the message.

                // Persist the regID - no need to register again.
                GcmUtils.storeRegistrationId(SettingsActivity.this, regid);
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
                // If there is an error, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off.
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String msg) {
            Log.e("Registration ID", msg);
        }
    }

}