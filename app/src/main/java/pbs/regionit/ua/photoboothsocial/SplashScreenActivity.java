package pbs.regionit.ua.photoboothsocial;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;

import pbs.regionit.ua.photoboothsocial.utils.InfinitiveDrawable;

public class SplashScreenActivity extends Activity {

    private static int SPLASH_TIME_OUT = 4000;
    int sdk = Build.VERSION.SDK_INT;
    LinearLayout bg_screen_layout;
    Bitmap bitmap;

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
// Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general rule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.splash_screen);

        BitmapFactory.Options o=new BitmapFactory.Options();
        o.inSampleSize = 1;
        o.inDither=false;                     //Disable Dithering mode
        o.inPurgeable=true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
        bitmap=BitmapFactory.decodeResource(getResources(),R.drawable.splash_background, o);

        bg_screen_layout = (LinearLayout) findViewById(R.id.bg_screen_layout);
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            bg_screen_layout.setBackgroundDrawable(
                    new InfinitiveDrawable(bitmap, getResources().getDisplayMetrics().widthPixels));
        } else {
            bg_screen_layout.setBackground(
                    new InfinitiveDrawable(bitmap,
                            getResources().getDisplayMetrics().widthPixels));
        }




        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onDestroy() {
        bitmap.recycle();
        bitmap = null;
        super.onDestroy();
    }
}
