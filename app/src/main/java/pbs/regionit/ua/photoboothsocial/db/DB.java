package pbs.regionit.ua.photoboothsocial.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import pbs.regionit.ua.photoboothsocial.utils.FileUtils;

/**
 * Created by Lotar on 14.12.2014.
 */
public class DB {

    public static final int VERSION = 3;
    public static final String NAME = "photoboothsocial";
    public static final String AUTHORITY = "pbs.regionit.ua.photoboothsocial";


    public static final String TABLE_IMAGES = "IMAGES";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_SERVER_ID = "server_id";
    public static final String FIELD_IMAGE_LINK_STD = "image_link_std";
    public static final String FIELD_IMAGE_LINK_MID = "image_link_mid";
    public static final String FIELD_IMAGE_LINK_LOW = "image_link_low";
    public static final String FIELD_IMAGE_LINK_THUMB = "image_link_thumb";
    public static final String FIELD_IMAGE_SHARED_VK = "image_shared_vk";
    public static final String FIELD_IMAGE_SHARED_FB = "image_shared_fb";
    public static String DB_FILEPATH = "/data/data/pbs.regionit.ua.photoboothsocial/databases/database.db";


    private static final String CREATE_IMAGES = "CREATE TABLE `"+ TABLE_IMAGES+ "` (" +
            "  `"+FIELD_ID+"` INTEGER PRIMARY KEY," +
            "  `"+FIELD_SERVER_ID+"` INTEGER NOT NULL,"+
            "  `"+FIELD_IMAGE_LINK_STD+"` VARCHAR(120) NOT NULL," +
            "  `"+FIELD_IMAGE_LINK_MID+"` VARCHAR(120) NOT NULL," +
            "  `"+FIELD_IMAGE_LINK_LOW+"` VARCHAR(120) NOT NULL," +
            "  `"+FIELD_IMAGE_LINK_THUMB+"` VARCHAR(120) NOT NULL," +
            "  `"+ FIELD_IMAGE_SHARED_VK +"` INTEGER DEFAULT 0," +
            "  `"+ FIELD_IMAGE_SHARED_FB +"` INTEGER DEFAULT 0" +
            ");";

    private final Context mCtx;


    private DBHelper mDBHelper;
    private SQLiteDatabase mDB;

    public DB(Context ctx) {
        mCtx = ctx;
    }

   /* public DBHelper getInstance(Context context) {
        if (dbHelper == null) {
            dbHelper = new DBHelper(context);
        }
        mDB = dbHelper.getWritableDatabase();
        return dbHelper;
    }*/

    // открыть подключение
    public void open() {
        if (mDB==null || !mDB.isOpen()) {
            Log.e("Database","open");
            mDBHelper = new DBHelper(mCtx);
            mDB = mDBHelper.getWritableDatabase();
            /*try {
                importDatabase("photobooth_backup.db");
            } catch (IOException e) {
                e.printStackTrace();
            }*/
        }
    }

    // закрыть подключение
    public void close() {
        if(mDB != null&& mDB.isOpen()){
            Log.e("Database","mDB close");
            mDB.close();
        }
        if (mDBHelper!=null){
            Log.e("Database","mDBHelper close");
            mDBHelper.close();
        }

    }

    // получить все данные из таблицы DB_TABLE
    public Cursor getAllData() {
        return mDB.query(TABLE_IMAGES, null, null, null, null, null, FIELD_SERVER_ID + " DESC");
    }

    // получить все данные из таблицы DB_TABLE
    public Cursor getAllBigPicture() {
        String[] columns = new String[] { FIELD_IMAGE_LINK_MID, FIELD_SERVER_ID };
        return mDB.query(TABLE_IMAGES, columns, null, null, null, null, FIELD_SERVER_ID + " DESC");
    }

    public Cursor getNotSharedPictureVk() {
        String[] columns = new String[] {FIELD_IMAGE_SHARED_VK, FIELD_IMAGE_LINK_MID };
        return mDB.query(TABLE_IMAGES, columns,FIELD_IMAGE_SHARED_VK +"=0", null, null, null, FIELD_SERVER_ID + " DESC");
    }

    public void updateImageSharedVk() {
        ContentValues values = new ContentValues();
        values.put(DB.FIELD_IMAGE_SHARED_VK, 1);
        mDB.update(TABLE_IMAGES, values, FIELD_IMAGE_SHARED_VK +"=0", null);
    }

    public Cursor getNotSharedPictureFb() {
        String[] columns = new String[] {FIELD_IMAGE_SHARED_FB, FIELD_IMAGE_LINK_MID };
        return mDB.query(TABLE_IMAGES, columns, FIELD_IMAGE_SHARED_FB +"=0", null, null, null, FIELD_SERVER_ID + " DESC");
    }

    public void updateImageSharedFb() {
        ContentValues values = new ContentValues();
        values.put(DB.FIELD_IMAGE_SHARED_FB, 1);
        mDB.update(TABLE_IMAGES, values, FIELD_IMAGE_SHARED_FB +"=0", null);
    }

    // добавить запись в DB_TABLE
    public void addRec(String serverId, String linkLow, String linkMid, String linkStd, String linkthumb) {
        ContentValues cv = new ContentValues();
        cv.put(FIELD_SERVER_ID, serverId);
        cv.put(FIELD_IMAGE_LINK_LOW, linkLow);
        cv.put(FIELD_IMAGE_LINK_MID, linkMid);
        cv.put(FIELD_IMAGE_LINK_STD, linkStd);
        cv.put(FIELD_IMAGE_LINK_THUMB, linkthumb);
        mDB.insert(TABLE_IMAGES, null, cv);


    }

    // удалить запись из DB_TABLE
    public void delRec(long id) {
       mDB.delete(TABLE_IMAGES, FIELD_ID + " = " + id, null);
    }


    public void delAllRec() {
        mDB.delete(TABLE_IMAGES,null, null);
    }


    public boolean importDatabase(String dbPath) throws IOException {
        // Close the SQLiteOpenHelper so it will commit the created empty
        // database to internal storage.
        close();
        File data = Environment.getDataDirectory();
        File newDb = new File(data, dbPath);
        File oldDb = new File(DB_FILEPATH);
        if (newDb.exists()) {
            FileUtils.copyFile(new FileInputStream(newDb), new FileOutputStream(oldDb));
            // Access the copied database so SQLiteHelper will cache it and mark
            // it as created.
            mDBHelper.getWritableDatabase().close();
            return true;
        }
        return false;
    }


    // класс по созданию и управлению БД
    public class DBHelper extends SQLiteOpenHelper {



        public DBHelper(Context context) {
            super(context, NAME, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_IMAGES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            if(oldVersion != newVersion){
                db.execSQL("DROP TABLE IF EXISTS `IMAGES`;");
                onCreate(db);
            }

        }
    }
}
