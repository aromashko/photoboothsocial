package pbs.regionit.ua.photoboothsocial;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.SocialNetworkManager;
import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
import com.github.gorbin.asne.core.listener.OnPostingCompleteListener;
import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.github.gorbin.asne.vk.VkSocialNetwork;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.vk.sdk.VKScope;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import pbs.regionit.ua.photoboothsocial.db.DB;
import pbs.regionit.ua.photoboothsocial.utils.Const;

/**
 * Created by RmIIIK on 11.11.2014.
 */
public class fbTab extends Fragment implements CompoundButton.OnCheckedChangeListener, SocialNetworkManager.OnInitializationCompleteListener, OnLoginCompleteListener, OnPostingCompleteListener {

    Switch swch_activate;
    EditText et_fb_login, et_fb_password, edt_share_text;
    CheckBox chb_save_login;
    Button btn_share;
    public SocialNetwork socialNetwork;
    public SocialNetworkManager mSocialNetworkManager;
    //private UiLifecycleHelper uiHelper;
    private int networkId = Const.FACEBOOK;
    String TAG = fbTab.class.getSimpleName();
    public String VK_KEY;
    DB db;
    protected ImageLoader imageLoader = ImageLoader.getInstance();

    /** Called when the activity is first created. */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //uiHelper = new UiLifecycleHelper(getActivity(), null);
        //uiHelper.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        //uiHelper.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //uiHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        //uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       // uiHelper.onDestroy();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.facebook_tab, container, false);
        initView(v);

        VK_KEY = getString(R.string.vk_app_id);

        edt_share_text.setText(getShareTextPrefs());
        edt_share_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                storePrefs(Const.FB_SHARE_TEXT, s.toString());
            }
        });

        //Chose permissions
        ArrayList<String> fbScope = new ArrayList<String>();
        fbScope.addAll(Arrays.asList("public_profile, email, user_friends"));

        String[] vkScope = new String[] {
                VKScope.WALL,
                VKScope.PHOTOS,
                VKScope.NOHTTPS,
        };

        //Use manager to manage SocialNetworks
        mSocialNetworkManager = (SocialNetworkManager) getFragmentManager().findFragmentByTag(Const.SOCIAL_NETWORK_TAG);

        //Check if manager exist
        if (mSocialNetworkManager == null) {
            mSocialNetworkManager = new SocialNetworkManager();

            VkSocialNetwork vkNetwork = new VkSocialNetwork(this, VK_KEY, vkScope);
            mSocialNetworkManager.addSocialNetwork(vkNetwork);

            //Init and add to manager FacebookSocialNetwork
            FacebookSocialNetwork fbNetwork = new FacebookSocialNetwork(fbTab.this, fbScope);
            mSocialNetworkManager.addSocialNetwork(fbNetwork);

            //Initiate every network from mSocialNetworkManager
            getFragmentManager().beginTransaction().add(mSocialNetworkManager, Const.SOCIAL_NETWORK_TAG).commit();
            mSocialNetworkManager.setOnInitializationCompleteListener(this);
        } else {
            //if manager exist - get and setup login only for initialized SocialNetworks
            if(!mSocialNetworkManager.getInitializedSocialNetworks().isEmpty()) {
                List<SocialNetwork> socialNetworks = mSocialNetworkManager.getInitializedSocialNetworks();
                for (SocialNetwork socialNetwork : socialNetworks) {

                    socialNetwork.setOnLoginCompleteListener(this);
                }
            }
        }
        db = ((SettingsActivity) getActivity()).getDb();
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("OnActivityResult", "fb");

        /*uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
            @Override
            public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
                Log.e("Activity", String.format("Error: %s", error.toString()));
            }

            @Override
            public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
                Log.i("Activity", "Success!");
            }
        });*/
    }

    public void initView(View v){
        swch_activate = (Switch) v.findViewById(R.id.swch_activate);
        btn_share = (Button) v.findViewById(R.id.btn_share);
        et_fb_login = (EditText) v.findViewById(R.id.et_fb_login);
        et_fb_password = (EditText) v.findViewById(R.id.et_fb_password);
        edt_share_text = (EditText) v.findViewById(R.id.edt_share_text);
        chb_save_login= (CheckBox) v.findViewById(R.id.chb_save_login);


        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                socialNetwork = mSocialNetworkManager.getSocialNetwork(networkId);
                if (socialNetwork.isConnected()) {
                    socialNetwork.logout();
                }
                loginMaker();
            }
        });
        swch_activate.setChecked(getActivatedPrefs());
        chb_save_login.setChecked(getSaveLoginPrefs());
        swch_activate.setOnCheckedChangeListener(this);
        chb_save_login.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.swch_activate:
                storePrefs(Const.FB_ACTIVATED, isChecked);
                break;
            case R.id.chb_save_login:
                storePrefs(Const.FB_SAVE_LOGIN, isChecked);
                break;
        }
    }

    public void storePrefs(String key, String value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void storePrefs(String key,boolean value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getActivatedPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.FB_ACTIVATED, false);
        return autoShare;
    }

    public boolean getSaveLoginPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.FB_SAVE_LOGIN, false);
        return autoShare;
    }

    public String getShareTextPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String shareText = prefs.getString(Const.FB_SHARE_TEXT, "");
        return shareText;
    }


    public void loginMaker() {
        socialNetwork = mSocialNetworkManager.getSocialNetwork(networkId);
        if(!socialNetwork.isConnected()) {
            ((SettingsActivity) getActivity()).showProgress("Loading social person");
            Log.e(TAG, "loginMaker");
            socialNetwork.requestLogin();
        }
        else{
            sharing();
        }
    }

    public void sharing(){
        Log.e(TAG, "Sharing");
        Cursor c = null;
        ArrayList<String> links = new ArrayList<String>();
        c = db.getNotSharedPictureFb();
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    db.updateImageSharedFb();
                    links.add(c.getString(c.getColumnIndex(DB.FIELD_IMAGE_LINK_MID)));
                    Log.e(TAG, c.getString(c.getColumnIndex(DB.FIELD_IMAGE_LINK_MID)));
                } while (c.moveToNext());
            }
            c.close();
        } else{
            Log.d(TAG, "Cursor is null");
        }
        if (links.size()==0){
            Toast toast =Toast.makeText(getActivity(), R.string.nothing_to_share, Toast.LENGTH_LONG);
            LinearLayout toastLayout = (LinearLayout) toast.getView();
            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(30);
            toast.show();
        }
        for (String link:links) {
            imageLoader.loadImage(link, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    Log.e(TAG, "onLoadingStarted");
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    Log.e(TAG, "onLoadingFailed");
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    Log.e(TAG, "onLoadingComplete");

                    File file = bitmapToFile(loadedImage);
                    try {
                        socialNetwork.requestPostPhoto(file,"", fbTab.this);
                    }
                    catch (NullPointerException e){
                        e.printStackTrace();
                        Toast toast =Toast.makeText(getActivity(), R.string.something_wrong, Toast.LENGTH_LONG);
                        LinearLayout toastLayout = (LinearLayout) toast.getView();
                        TextView toastTV = (TextView) toastLayout.getChildAt(0);
                        toastTV.setTextSize(30);
                        toast.show();
                    }


                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    Log.e(TAG, "onLoadingCancelled");
                }
            });
        }
    }

    public File bitmapToFile(Bitmap bitmap){
        File f = new File(getActivity().getCacheDir(), Calendar.getInstance().getTimeInMillis()+".png");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    @Override
    public void onSocialNetworkManagerInitialized() {
        for (SocialNetwork socialNetwork : mSocialNetworkManager.getInitializedSocialNetworks()) {
            Log.e(TAG, "onSocialNetworkManagerInitialized setOnLoginCompleteListener");
            socialNetwork.setOnLoginCompleteListener(this);
        }
        Log.e(TAG, "SocialNetworkManagerInitialized");
    }

    @Override
    public void onLoginSuccess(int i) {
       Log.e(TAG, "LoginSuccess");
        ((SettingsActivity) getActivity()).hideProgress();
        sharing();
    }

    @Override
    public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {
        Log.e(TAG, "socialNetworkID= " + socialNetworkID + ",requestID= " + requestID + ",errorMessage= " + errorMessage + ",data= " + data);
        ((SettingsActivity) getActivity()).hideProgress();
    }

    @Override
    public void onPostSuccessfully(int socialNetworkID) {
        Toast toast =Toast.makeText(getActivity(), R.string.share_successfully_fb, Toast.LENGTH_LONG);
        LinearLayout toastLayout = (LinearLayout) toast.getView();
        TextView toastTV = (TextView) toastLayout.getChildAt(0);
        toastTV.setTextSize(30);
        toast.show();
        Log.e(TAG, "onPostSuccessfully");
        try {
            socialNetwork.logout();
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
    }
}