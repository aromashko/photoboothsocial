package pbs.regionit.ua.photoboothsocial.fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.SocialNetworkException;
import com.github.gorbin.asne.core.SocialNetworkManager;
import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
import com.github.gorbin.asne.core.listener.OnPostingCompleteListener;
import com.github.gorbin.asne.core.listener.OnRequestSocialPersonCompleteListener;
import com.github.gorbin.asne.core.persons.SocialPerson;
import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.github.gorbin.asne.instagram.InstagramSocialNetwork;
import com.github.gorbin.asne.vk.VkSocialNetwork;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.vk.sdk.VKScope;

import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pbs.regionit.ua.photoboothsocial.MainActivity;
import pbs.regionit.ua.photoboothsocial.R;
import pbs.regionit.ua.photoboothsocial.utils.Const;
import pbs.regionit.ua.photoboothsocial.utils.EmailFormatValidator;
import pbs.regionit.ua.photoboothsocial.utils.Utils;
import pbs.regionit.ua.photoboothsocial.utils.ZoomOutPageTransformer;

public class PhotoFullFragment extends Fragment implements SocialNetworkManager.OnInitializationCompleteListener, OnLoginCompleteListener, OnRequestSocialPersonCompleteListener {
    String TAG = PhotoFullFragment.class.getSimpleName();
    static ArrayList<String> mListOfFullImageLinks = new ArrayList<String>();
    static ArrayList<String> mListOfId = new ArrayList<String>();
    ViewPager pager;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    int margin;
    static int mPosition;
    public static SocialNetworkManager mSocialNetworkManager;
    private String VK_KEY;
    private int networkId = 0;
    SocialNetwork socialNetwork;
    public ProgressDialog pd;
    //private UiLifecycleHelper uiHelper;
    DisplayImageOptions options;
    int currentViewPagerPosition;
    AsyncHttpClient client;
    EditText input;
    EmailFormatValidator emailFormatValidator;
    boolean isFacebookActivated, isVkActivated, isSmsActivated, isMailActivated,
            isPrintActivated, isInstargramActivated;
    FrameLayout frameMain;
    String backgroundPath;
    int sdk;
    Button btn_logout;
    //BroadcastReceiver receiver;

    public static PhotoFullFragment newInstance(ArrayList<String> listOfFullImageLinks,ArrayList<String> listOfId,  int position) {
        mListOfFullImageLinks.clear();
        mListOfFullImageLinks.addAll(listOfFullImageLinks);
        mListOfId.clear();
        mListOfId.addAll(listOfId);
        mPosition = position;
        PhotoFullFragment fragment = new PhotoFullFragment();
        return fragment;
    }
    public PhotoFullFragment() {
        // Required empty public constructor
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        initializeSocialNetwork();
        View rootView = inflater.inflate(R.layout.photo_full_fragment, container, false);
        settingImageLoader();
        if (isNeedLogout()){
            clearCache();
        }

        pager = (ViewPager) rootView.findViewById(R.id.pager);
        frameMain = (FrameLayout) rootView.findViewById(R.id.frameMain);
        btn_logout= (Button) rootView.findViewById(R.id.btn_logout);


        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutSocialNetwork();
            }
        });
        installationViewPager();
        pager.setAdapter(new ImagePagerAdapter(mListOfFullImageLinks));
        pager.setCurrentItem(mPosition);
        client = new AsyncHttpClient();
        emailFormatValidator = new EmailFormatValidator();
        isActivatedSocialNetwork();

        return rootView;
    }



    public void isActivatedSocialNetwork(){
        isFacebookActivated = isFacebookActivated();
        isVkActivated = isVkActivated();
        isMailActivated = isMailActivated();
        isSmsActivated = isSmsActivated();
        isPrintActivated = isPrintActivated();
        isInstargramActivated = isInstagramActivated();
    }


    public void initializeSocialNetwork(){
        VK_KEY = getString(R.string.vk_app_id);
        ArrayList<String> fbScope = new ArrayList<String>();
        fbScope.addAll(Arrays.asList("public_profile, email, user_friends"));

        String[] vkScope = new String[] {
                VKScope.WALL,
                VKScope.PHOTOS,
                VKScope.NOHTTPS
        };

        mSocialNetworkManager = (SocialNetworkManager) getFragmentManager().findFragmentByTag(Const.SOCIAL_NETWORK_TAG);
        if (mSocialNetworkManager == null) {
            mSocialNetworkManager = new SocialNetworkManager();

            //Init and add to manager FacebookSocialNetwork
            FacebookSocialNetwork fbNetwork = new FacebookSocialNetwork(this, fbScope);
            mSocialNetworkManager.addSocialNetwork(fbNetwork);

            VkSocialNetwork vkNetwork = new VkSocialNetwork(this, VK_KEY, vkScope);
            mSocialNetworkManager.addSocialNetwork(vkNetwork);


            InstagramSocialNetwork instagramSocialNetwork= new InstagramSocialNetwork(this, getString(R.string.instagram_client_id),
                    getString(R.string.instagram_client_secret), getString(R.string.instagram_redirect_url), "comments");
            mSocialNetworkManager.addSocialNetwork(instagramSocialNetwork);

            //Initiate every network from mSocialNetworkManager
            getFragmentManager().beginTransaction().add(mSocialNetworkManager, Const.SOCIAL_NETWORK_TAG).commit();
            mSocialNetworkManager.setOnInitializationCompleteListener(this);
        } else {
            //if manager exist - get and setup login only for initialized SocialNetworks
            if(!mSocialNetworkManager.getInitializedSocialNetworks().isEmpty()) {
                List<SocialNetwork> socialNetworks = mSocialNetworkManager.getInitializedSocialNetworks();
                for (SocialNetwork socialNetwork : socialNetworks) {

                    socialNetwork.setOnLoginCompleteListener(this);
                }
            }
        }
    }

    public void installationViewPager() {
        //margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120 * 2, getResources().getDisplayMetrics());
        pager.setPageTransformer(true, new ZoomOutPageTransformer());

        //pager.setPageMargin(-margin);
        pager.setClipChildren(false);
        pager.setOffscreenPageLimit(3);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentViewPagerPosition = position;
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });
    }


    @Override
    public void onSocialNetworkManagerInitialized() {
        Log.e(TAG, "SocialNetworkManagerInitialized");
        for (SocialNetwork socialNetwork : mSocialNetworkManager.getInitializedSocialNetworks()) {
            Log.e(TAG, "onSocialNetworkManagerInitialized setOnLoginCompleteListener");
            socialNetwork.setOnLoginCompleteListener(this);
        }
        socialNetwork = mSocialNetworkManager.getSocialNetwork(Const.FACEBOOK);
        boolean isFbConnected= socialNetwork.isConnected();
        Log.e("Facebook connected",isFbConnected+"");
        socialNetwork = mSocialNetworkManager.getSocialNetwork(Const.VKONTAKTE);
        boolean isVkConnected= socialNetwork.isConnected();
        Log.e("Vk connected", isVkConnected+"");
        if (isFbConnected || isVkConnected)
            btn_logout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoginSuccess(int i) {
        Log.e(TAG, "LoginSuccess");
        hideProgress();
        sharing();
        btn_logout.setVisibility(View.VISIBLE);

    }

    public void sharing(){
        Log.e(TAG, "Sharing");
        File file =imageLoader.getDiskCache().get(mListOfFullImageLinks.get(currentViewPagerPosition));
        switch (networkId){
            case Const.FACEBOOK:
                if (isNeedSaveFBStatistics()){
                socialNetwork.setOnRequestCurrentPersonCompleteListener(this);
                socialNetwork.requestCurrentPerson();
                }
                socialNetwork.requestPostPhoto(file, "", postingComplete);
                break;
            case Const.VKONTAKTE:
                if (isNeedSaveVkStatistics()){
                    socialNetwork.setOnRequestCurrentPersonCompleteListener(this);
                    socialNetwork.requestCurrentPerson();
                }
                socialNetwork.requestPostPhoto(file, getVkShareText(), postingComplete);
                break;
            case Const.INSTAGRAM:
                //socialNetwork.setOnRequestCurrentPersonCompleteListener(this);
                //socialNetwork.requestCurrentPerson();

                socialNetwork.requestPostPhoto(file, getInstagramShareText(), postingComplete);
                break;
        }
    }

    public String getInstagramShareText() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String shareText = prefs.getString(Const.INSTAGRAM_SHARE_TEXT, "");
        return shareText;
    }


    public String getVkShareText() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String shareText = prefs.getString(Const.VK_SHARE_TEXT, "");
        return shareText;
    }


    public boolean isNeedSaveVkStatistics() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean isNeedSave = prefs.getBoolean(Const.VK_SAVE_LOGIN, false);
        return isNeedSave;
    }

    public boolean isNeedSaveFBStatistics() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean isNeedSave = prefs.getBoolean(Const.FB_SAVE_LOGIN, false);
        return isNeedSave;
    }

    public boolean isFacebookActivated() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean isActivated = prefs.getBoolean(Const.FB_ACTIVATED, false);
        return isActivated;
    }

    public boolean isVkActivated() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean isActivated = prefs.getBoolean(Const.VK_ACTIVATED, false);
        return isActivated;
    }

    public boolean isMailActivated() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean isActivated = prefs.getBoolean(Const.MAIL_ACTIVATED, false);
        return isActivated;
    }


    public boolean isSmsActivated() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean isActivated = prefs.getBoolean(Const.SMS_ACTIVATED, false);
        return isActivated;
    }

    public boolean isPrintActivated() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean isActivated = prefs.getBoolean(Const.PRINT_ACTIVATED, false);
        return isActivated;
    }



    public void saveStatistics(String networkName, String id){
        JSONObject jsonObject=null;
        ByteArrayEntity entity = null;

            try {
                jsonObject = new JSONObject();
                jsonObject.put("registrationid", ((MainActivity) getActivity()).getRegid());
                JSONArray jsonArray = new JSONArray();
                JSONObject queryObj = new JSONObject();
                queryObj.put("type", "saveinfo");
                queryObj.put("fileid", mListOfId.get(currentViewPagerPosition));
                queryObj.put("way", networkName);
                queryObj.put("account", id);

                jsonArray.put(queryObj);
                jsonObject.put("query", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("jsonObject", jsonObject.toString());
            try {
                entity = new ByteArrayEntity(jsonObject.toString().getBytes("UTF-8"));
            }
            catch(UnsupportedEncodingException e){
                e.printStackTrace();

            }

            client.post(getActivity(), getResources().getString(R.string.api_url), entity, "application/json",  new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(JSONObject response) {
                    Log.d(TAG, String.valueOf(response));
                    try {
                        JSONArray array = response.getJSONArray("response");
                        String status = array.getJSONObject(0).getString("status");
                        String type = array.getJSONObject(0).getString("type");
                        Log.d(TAG, status);
                        Log.d(TAG, type);

                       /* if (status.equals("true") && type.equals("saveinfo")){
                            //Toast.makeText(getActivity(), "Статистика успешно записана", Toast.LENGTH_LONG).show();
                        }
                        else{
                            //Toast.makeText(getActivity(), "Статистика не записана", Toast.LENGTH_LONG).show();
                        }*/

                    } catch (JSONException e) {
                        Log.w(TAG, "Parsing error", e);
                    }
                }

                @Override
                public void onFailure(Throwable e, JSONObject errorResponse) {
                    super.onFailure(e, errorResponse);
                }
            });
        }


    public void sendEmail(String email){
        JSONObject jsonObject=null;
        ByteArrayEntity entity = null;

        try {
            String emailShareText = getMailShareTextPrefs();
            jsonObject = new JSONObject();
            jsonObject.put("registrationid", ((MainActivity) getActivity()).getRegid());
            JSONArray jsonArray = new JSONArray();
            JSONObject queryObj = new JSONObject();
            queryObj.put("type", "sendemail");
            queryObj.put("fileid", mListOfId.get(currentViewPagerPosition));
            queryObj.put("email", email);
            if (!emailShareText.isEmpty()){
                queryObj.put("emailhtml", emailShareText);
            }

            jsonArray.put(queryObj);
            jsonObject.put("query", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("jsonObject", jsonObject.toString());
        try {
            entity = new ByteArrayEntity(jsonObject.toString().getBytes("UTF-8"));
        }
        catch(UnsupportedEncodingException e){
            e.printStackTrace();

        }

        client.post(getActivity(), getResources().getString(R.string.api_url), entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, String.valueOf(response));
                try {
                    JSONArray array = response.getJSONArray("response");
                    String status = array.getJSONObject(0).getString("status");
                    String type = array.getJSONObject(0).getString("type");
                    Log.d(TAG, status);
                    Log.d(TAG, type);

                    if (status.equals("true") && type.equals("sendemail")) {
                        Toast toast =Toast.makeText(getActivity(), R.string.mail_send_successfully, Toast.LENGTH_LONG);
                        LinearLayout toastLayout = (LinearLayout) toast.getView();
                        TextView toastTV = (TextView) toastLayout.getChildAt(0);
                        toastTV.setTextSize(30);
                        toast.show();
                    } else {
                        Toast toast =Toast.makeText(getActivity(),R.string.mail_dont_send, Toast.LENGTH_LONG);
                        LinearLayout toastLayout = (LinearLayout) toast.getView();
                        TextView toastTV = (TextView) toastLayout.getChildAt(0);
                        toastTV.setTextSize(30);
                        toast.show();
                    }

                } catch (JSONException e) {
                    Log.w(TAG, "Parsing error", e);
                }
            }

            @Override
            public void onFailure(Throwable e, JSONObject errorResponse) {
                super.onFailure(e, errorResponse);
            }
        });
    }

    public void clearCache(){
        imageLoader.clearDiskCache();
    }

    public void logoutSocialNetwork(){
        btn_logout.setVisibility(View.GONE);
        storePrefs(Const.NEED_LOGOUT, false);
        try {
            socialNetwork = mSocialNetworkManager.getSocialNetwork(Const.FACEBOOK);
            socialNetwork.logout();
            socialNetwork = mSocialNetworkManager.getSocialNetwork(Const.VKONTAKTE);
            socialNetwork.logout();
        }
        catch (SocialNetworkException e){
            e.printStackTrace();
        }
        /*socialNetwork = mSocialNetworkManager.getSocialNetwork(Const.INSTAGRAM);
        socialNetwork.logout();*/
    }

    public void storePrefs(String key,boolean value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }


    public void sendSms(String phoneNumber){
        String smsShareText= getSmsShareTextPrefs();
        JSONObject jsonObject=null;
        ByteArrayEntity entity = null;

        try {
            jsonObject = new JSONObject();
            jsonObject.put("registrationid", ((MainActivity) getActivity()).getRegid());
            JSONArray jsonArray = new JSONArray();
            JSONObject queryObj = new JSONObject();
            queryObj.put("type", "sendsms");
            queryObj.put("fileid", mListOfId.get(currentViewPagerPosition));
            queryObj.put("mobile", phoneNumber);
            if (!smsShareText.isEmpty()) {
                queryObj.put("text", smsShareText);
            }
            jsonArray.put(queryObj);
            jsonObject.put("query", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("jsonObject", jsonObject.toString());
        try {
            entity = new ByteArrayEntity(jsonObject.toString().getBytes("UTF-8"));
        }
        catch(UnsupportedEncodingException e){
            e.printStackTrace();

        }

        client.post(getActivity(), getResources().getString(R.string.api_url), entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, String.valueOf(response));
                try {
                    JSONArray array = response.getJSONArray("response");
                    String status = array.getJSONObject(0).getString("status");
                    Log.d(TAG, status);

                    if (status.equals("true")) {
                        Toast toast =Toast.makeText(getActivity(), R.string.sms_send_successfully, Toast.LENGTH_LONG);
                        LinearLayout toastLayout = (LinearLayout) toast.getView();
                        TextView toastTV = (TextView) toastLayout.getChildAt(0);
                        toastTV.setTextSize(30);
                        toast.show();
                    } else {
                        Toast toast =Toast.makeText(getActivity(), R.string.sms_dont_send, Toast.LENGTH_LONG);
                        LinearLayout toastLayout = (LinearLayout) toast.getView();
                        TextView toastTV = (TextView) toastLayout.getChildAt(0);
                        toastTV.setTextSize(30);
                        toast.show();
                    }

                } catch (JSONException e) {
                    Log.w(TAG, "Parsing error", e);
                }
            }

            @Override
            public void onFailure(Throwable e, JSONObject errorResponse) {
                super.onFailure(e, errorResponse);
            }
        });
    }


    public void print(){
        String smsShareText= getSmsShareTextPrefs();
        JSONObject jsonObject=null;
        ByteArrayEntity entity = null;

        try {
            jsonObject = new JSONObject();
            jsonObject.put("registrationid", ((MainActivity) getActivity()).getRegid());
            JSONArray jsonArray = new JSONArray();
            JSONObject queryObj = new JSONObject();
            queryObj.put("type", "print");
            queryObj.put("id", mListOfId.get(currentViewPagerPosition));
            jsonArray.put(queryObj);
            jsonObject.put("query", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("jsonObject", jsonObject.toString());
        try {
            entity = new ByteArrayEntity(jsonObject.toString().getBytes("UTF-8"));
        }
        catch(UnsupportedEncodingException e){
            e.printStackTrace();

        }

        client.post(getActivity(), getResources().getString(R.string.api_url), entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, String.valueOf(response));
                try {
                    JSONArray array = response.getJSONArray("response");
                    String status = array.getJSONObject(0).getString("status");
                    Log.d(TAG, status);

                    if (status.equals("true")) {
                        Toast toast =Toast.makeText(getActivity(), R.string.print_successfull, Toast.LENGTH_LONG);
                        LinearLayout toastLayout = (LinearLayout) toast.getView();
                        TextView toastTV = (TextView) toastLayout.getChildAt(0);
                        toastTV.setTextSize(30);
                        toast.show();
                    } else {
                        Toast toast =Toast.makeText(getActivity(), String.valueOf(response), Toast.LENGTH_LONG);
                        LinearLayout toastLayout = (LinearLayout) toast.getView();
                        TextView toastTV = (TextView) toastLayout.getChildAt(0);
                        toastTV.setTextSize(30);
                        toast.show();
                    }

                } catch (JSONException e) {
                    Log.w(TAG, "Parsing error", e);
                }
            }

            @Override
            public void onFailure(Throwable e, JSONObject errorResponse) {
                super.onFailure(e, errorResponse);
            }
        });
    }



    public void settingImageLoader(){
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        //uiHelper.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //uiHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        //uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //uiHelper.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("OnActivityResult", "PhotoFullFragment");

       /* uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
            @Override
            public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
                Log.e("Activity", String.format("Error: %s", error.toString()));
            }

            @Override
            public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
                Log.i("Activity", "Success!");
            }
        });*/
    }



    @Override
    public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {
        Log.e(TAG, "socialNetworkID= " + socialNetworkID + ",requestID= " + requestID + ",errorMessage= " + errorMessage + ",data= " + data);
        hideProgress();
    }

    public void loginMaker() {
        Log.e(TAG, "loginMaker");
        socialNetwork = mSocialNetworkManager.getSocialNetwork(networkId);
        if(!socialNetwork.isConnected()) {
            if(networkId != 0) {
                showProgress("Loading social person");
                socialNetwork.requestLogin();
            } else {
                hideProgress();
                Toast.makeText(getActivity(), "Wrong networkId", Toast.LENGTH_LONG).show();
            }
        } else {
            sharing();
        }
    }


    private AlertDialog.Builder alertDialogInit(String title, String hint, int inputType){
        AlertDialog.Builder ad = new AlertDialog.Builder(getActivity());
        ad.setTitle(title);
        float textSize = Utils.dpToPx(getActivity(), 24);
            input = new EditText(getActivity());
            input.setInputType(inputType);
            input.setHint(hint);
            input.setTextSize(textSize);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            input.setLayoutParams(lp);
            ad.setView(input);
        ad.setPositiveButton("OK",
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        //Do nothing here because we override this button later to change the close behaviour.
                        //However, we still need this because on older versions of Android unless we
                        //pass a handler the button doesn't get instantiated
                    }
                });
        ad.setCancelable(true);
        return ad;
    }

    public void showProgress(String message) {
        pd = new ProgressDialog(getActivity());
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage(message);
        pd.setCancelable(true);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    public void hideProgress() {
        if (pd != null) {
            pd.dismiss();
        }
    }

    public boolean isInstagramActivated() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.INSTAGRAM_ACTIVATED, false);
        return autoShare;
    }

    public String getMailShareTextPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String shareText = prefs.getString(Const.MAIL_SHARE_TEXT, "");
        return shareText;
    }

    public String getSmsShareTextPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String shareText = prefs.getString(Const.SMS_SHARE_TEXT, "");
        return shareText;
    }

    public String getSmsPrefyksPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String sms_phonenumber_prefyks = prefs.getString(Const.SMS_PHONENUMBER_PREFYKS, "");
        return sms_phonenumber_prefyks;
    }

    public boolean isNeedLogout() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean isNeedLogout = prefs.getBoolean(Const.NEED_LOGOUT, false);
        return isNeedLogout;
    }

    @Override
    public void onRequestSocialPersonSuccess(int socialNetworkId, SocialPerson socialPerson) {
        String socialNetworkName="";
        String socialPersonId="";
        switch (socialNetworkId){
            case Const.FACEBOOK:
                socialNetworkName = "fb";
                socialPersonId = socialPerson.email;
                break;
            case Const.VKONTAKTE:
                socialNetworkName = "vk";
                socialPersonId = socialPerson.id;
                break;
            case Const.INSTAGRAM:
                socialNetworkName = "instagram";
                socialPersonId = socialPerson.id;
                Log.e("SocialPersonId", socialPersonId);
                break;
        }
        saveStatistics(socialNetworkName, socialPersonId);
        Log.e(TAG, "name" + socialPerson.name);
        Log.e(TAG, "id" + socialPerson.id);
        Log.e(TAG, "email" + socialPerson.email);
        String socialPersonString = socialPerson.toString();
        Log.e(TAG, "socialPersonString" + socialPersonString);
    }


    private class ImagePagerAdapter extends PagerAdapter{

        private ArrayList<String> images = new ArrayList<String>();
        private LayoutInflater inflater;
        ViewHolder holder;
        View view_pager_lay;

        public class ViewHolder {
            ImageView photo;
            ImageButton btnShareFacebook, btnShareVk, btnShareMail, btnShareSms,
                    btnShareInstagram, btnPrint;
        }


        ImagePagerAdapter(ArrayList<String> images) {
            this.images.clear();
            this.images.addAll(images);
            Log.d(TAG,  this.images.size() + " links size");
            inflater = getActivity().getLayoutInflater();
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return images.size();
        }

        public void activateShareButton(){
            if (isFacebookActivated){
                holder.btnShareFacebook.setVisibility(View.VISIBLE);
            }
            if (isVkActivated){
                holder.btnShareVk.setVisibility(View.VISIBLE);
            }
            if (isMailActivated){
                holder.btnShareMail.setVisibility(View.VISIBLE);
            }
            if (isSmsActivated){
                holder.btnShareSms.setVisibility(View.VISIBLE);
            }
            if (isPrintActivated){
                holder.btnPrint.setVisibility(View.VISIBLE);
            }
            if (isInstargramActivated){
                holder.btnShareInstagram.setVisibility(View.VISIBLE);
            }
           /* if (isFacebookActivated || isVkActivated){
                holder.btn_logout.setVisibility(View.VISIBLE);
            }*/
        }

        @Override
        public Object instantiateItem(ViewGroup view, final int position) {

            view_pager_lay = inflater.inflate(R.layout.view_pager_item, null);
            holder = new ViewHolder();
            holder.photo = (ImageView) view_pager_lay.findViewById(R.id.iv_photo);
            holder.btnShareMail= (ImageButton) view_pager_lay.findViewById(R.id.btnShareMail);
            holder.btnShareSms= (ImageButton) view_pager_lay.findViewById(R.id.btnShareSms);
            holder.btnShareFacebook = (ImageButton) view_pager_lay.findViewById(R.id.btnShareFacebook);
            holder.btnShareVk = (ImageButton) view_pager_lay.findViewById(R.id.btnShareVk);
            holder.btnShareInstagram = (ImageButton) view_pager_lay.findViewById(R.id.btnShareInstagram);
            holder.btnPrint = (ImageButton) view_pager_lay.findViewById(R.id.btnPrint);
           activateShareButton();

            holder.btnShareFacebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Need to be not in onCreate because getApllicationContext return null
                    // in onCreate
                    if (isNeedLogout()){
                    logoutSocialNetwork();
                    }
                    networkId = Const.FACEBOOK;
                    loginMaker();
                }
            });

            holder.btnShareVk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Need to be not in onCreate because getApllicationContext return null
                    // in onCreate
                    if (isNeedLogout()){
                    logoutSocialNetwork();
                    }
                    networkId = Const.VKONTAKTE;
                    loginMaker();
                }
            });

            holder.btnShareInstagram.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    networkId = Const.INSTAGRAM;
                    loginMaker();
                }
            });

            holder.btnShareMail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showEmailAlertDialog();
                }

            });
            holder.btnShareSms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showSmsAlertDialog();
                }
            });
            holder.btnPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   print();
                }
            });

            view_pager_lay.setTag(holder);

            imageLoader.displayImage(images.get(position), holder.photo, options);


            /*imageLoader.loadImage(images.get(position),  options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    Bitmap resizedBitmap;
                    if (position %2 ==0) {
                        resizedBitmap = Bitmap.createScaledBitmap(bitmap, 1800, 1200, true);
                    }
                    else{
                        resizedBitmap = Bitmap.createScaledBitmap(bitmap, 1200, 1800, true);
                    }
                    holder.photo.setImageBitmap(resizedBitmap);
                    resizedBitmap.recycle();
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });*/

            //Log.e(TAG,"path "+ path);

            view.addView(view_pager_lay);
            return view_pager_lay;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }


    }


    public void showEmailAlertDialog(){
        final AlertDialog.Builder ad = alertDialogInit(getString(R.string.enter_mail_dialog_title),
                getString(R.string.enter_mail_dialog_hint), InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        ad.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.cancel();
            }
        });
        final AlertDialog dialog = ad.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = input.getText().toString();
                if (emailFormatValidator.validate(email)) {
                    sendEmail(email);
                    dialog.cancel();
                } else {
                    Toast toast =Toast.makeText(getActivity(), R.string.wrong_mail, Toast.LENGTH_LONG);
                    LinearLayout toastLayout = (LinearLayout) toast.getView();
                    TextView toastTV = (TextView) toastLayout.getChildAt(0);
                    toastTV.setTextSize(30);
                    toast.show();

                }
            }
        });
    }


    public void showSmsAlertDialog(){
        String smsPrefyks = getSmsPrefyksPrefs();
        final AlertDialog.Builder ad = alertDialogInit(getString(R.string.enter_sms_dialog_title),
                getString(R.string.enter_sms_dialog_hint), InputType.TYPE_CLASS_PHONE);
        input.setText(smsPrefyks);
        input.setSelection(input.getText().length());
        ad.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.cancel();
            }
        });
        final AlertDialog dialog = ad.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = input.getText().toString();
                if (Patterns.PHONE.matcher(phoneNumber).matches()) {
                    sendSms(phoneNumber);
                    dialog.cancel();
                } else {
                    Toast toast =Toast.makeText(getActivity(), R.string.wrong_number, Toast.LENGTH_LONG);
                    LinearLayout toastLayout = (LinearLayout) toast.getView();
                    TextView toastTV = (TextView) toastLayout.getChildAt(0);
                    toastTV.setTextSize(30);
                    toast.show();
                }
            }
        });
    }

    private OnPostingCompleteListener postingComplete = new OnPostingCompleteListener() {
        @Override
        public void onPostSuccessfully(int socialNetworkID) {
            Log.e(TAG, "Post succesfully");
            Toast toast;
            switch (networkId){
                case Const.FACEBOOK:
                    toast =Toast.makeText(getActivity(), R.string.share_successfully_fb, Toast.LENGTH_LONG);
                    break;
                case Const.VKONTAKTE:
                    toast =Toast.makeText(getActivity(), R.string.share_successfully_vk, Toast.LENGTH_LONG);
                    break;
                default:
                    toast =Toast.makeText(getActivity(), "Успешно зашарено", Toast.LENGTH_LONG);
                    break;
            }
            LinearLayout toastLayout = (LinearLayout) toast.getView();
            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(30);
            toast.show();
        }

        @Override
        public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {
            hideProgress();
            Log.e("TAG", "socialNetworkID= "+  socialNetworkID + ",requestID= "+  requestID+ ",errorMessage= "+  errorMessage + ",data= "+  data);
        }
    };
}
