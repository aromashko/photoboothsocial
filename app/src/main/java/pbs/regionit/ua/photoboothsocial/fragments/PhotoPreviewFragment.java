package pbs.regionit.ua.photoboothsocial.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.commonsware.cwac.camera.CameraFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import pbs.regionit.ua.photoboothsocial.IOnFragmentInteractionListener;
import pbs.regionit.ua.photoboothsocial.MainActivity;
import pbs.regionit.ua.photoboothsocial.R;
import pbs.regionit.ua.photoboothsocial.SettingsActivity;
import pbs.regionit.ua.photoboothsocial.db.DB;
import pbs.regionit.ua.photoboothsocial.utils.Const;

public class PhotoPreviewFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    String TAG = PhotoPreviewFragment.class.getSimpleName();
    GridView gridPhoto;
    DB db;
    private SimpleCursorAdapter scAdapter;
    Cursor cursor;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    Button test_button_add_rec, test_button_delete_all_rec, btn_start_camera;
    AsyncHttpClient client;

    private IOnFragmentInteractionListener mListener;
    DisplayImageOptions options;
    Button settings, btn_update;
    EditText input;


    public static PhotoPreviewFragment newInstance() {
        PhotoPreviewFragment fragment = new PhotoPreviewFragment();
        return fragment;
    }
    public PhotoPreviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.photo_preview_fragment, container, false);
        initView(rootView);
        settingAdapter();
        settingImageLoader();
        client = new AsyncHttpClient();
        return rootView;
    }

    public void settingAdapter(){
        db = ((MainActivity) getActivity()).getDb();
        cursor = db.getAllData();
        // формируем столбцы сопоставления
        String[] from = new String[] { DB.FIELD_IMAGE_LINK_THUMB };
        int[] to = new int[] { R.id.iv_photo };

        // создааем адаптер и настраиваем список
        scAdapter = new SimpleCursorAdapter(getActivity(), R.layout.grid_view_item, cursor, from, to);
        scAdapter.setViewBinder(new GridViewBinder());
        gridPhoto.setAdapter(scAdapter);
       getActivity().getSupportLoaderManager().initLoader(0, null, this);
    }

    public void settingImageLoader(){
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true) // default
                .cacheOnDisk(true) // default
                .build();
    }

    public void initView(View v){

        gridPhoto = (GridView) v.findViewById(R.id.gridPhoto);
        test_button_add_rec = (Button) v.findViewById(R.id.test_button_add_rec);
        test_button_delete_all_rec= (Button) v.findViewById(R.id.test_button_delete_all_rec);
        btn_update  = (Button) v.findViewById(R.id.btn_update);
        btn_start_camera= (Button) v.findViewById(R.id.btn_start_camera);
        btn_start_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CameraFragment cameraFragment= new CameraFragment();
                ((MainActivity) getActivity()).navigateToFragment(cameraFragment,true);
            }
        });


        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update();
            }
        });
        settings = (Button) v.findViewById(R.id.btn_settings);
        settings.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (settings.getAlpha()<1) {
                    settings.setAlpha(1);
                    btn_update.setVisibility(View.VISIBLE);
                    settings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setClass(getActivity(), SettingsActivity.class);
                            startActivity(intent);
                            //showPinAlertDialog();

                        }
                    });
                }else{
                    settings.setAlpha(0);
                    btn_update.setVisibility(View.GONE);
                    settings.setOnClickListener(null);
                }
                return true;
            }
        });

        gridPhoto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor c;
                ArrayList<String> links = new ArrayList<String>();
                ArrayList<String> arrayOfId = new ArrayList<String>();
                c = db.getAllBigPicture();
                if (c != null) {
                    if (c.moveToFirst()) {
                        do {
                            links.add(c.getString(c.getColumnIndex(DB.FIELD_IMAGE_LINK_MID)));
                            arrayOfId.add(c.getString(c.getColumnIndex(DB.FIELD_SERVER_ID)));
                            Log.e(TAG, c.getString(c.getColumnIndex(DB.FIELD_IMAGE_LINK_MID)));
                        } while (c.moveToNext());
                    }
                    c.close();
                } else{
                    Log.d(TAG, "Cursor is null");
                }

                ((MainActivity) getActivity()).navigateToFragment(PhotoFullFragment.newInstance(links, arrayOfId, position),true);
                Log.d(TAG, links.size()+" links size");
            }
        });

        test_button_add_rec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.addRec("1514", "","http://admin.express-foto.info/media/mid/cf5e93c9f71d5c4eb1a514d7d0b2ee2a.jpg","http://admin.express-foto.info/media/cf5e93c9f71d5c4eb1a514d7d0b2ee2a.jpg", "http://admin.express-foto.info/media/thumb/a4fd377bd0737d1c1fa6d8fcf638b64c.jpg");
               getActivity().getSupportLoaderManager().getLoader(0).forceLoad();
            }
        });
        test_button_delete_all_rec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.delAllRec();
                getActivity().getSupportLoaderManager().getLoader(0).forceLoad();
            }
        });


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (IOnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new MyCursorLoader(getActivity(), db);
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> cursorLoader, Cursor cursor) {
        scAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> cursorLoader) {
        scAdapter.swapCursor(null);
    }





    public void showPinAlertDialog(){
        final AlertDialog.Builder ad = alertDialogInit(getString(R.string.pin_alert_title), getString(R.string.pin_alert_hint), InputType.TYPE_CLASS_NUMBER);
        ad.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.cancel();
            }
        });
        final AlertDialog dialog = ad.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String savedPincode = getSavedPinCode();
                String pinCode = input.getText().toString();
                if (pinCode.equals(savedPincode)) {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), SettingsActivity.class);
                    startActivity(intent);
                    dialog.cancel();
                } else {
                    Toast toast =Toast.makeText(getActivity(), R.string.wrong_pincode, Toast.LENGTH_LONG);
                    LinearLayout toastLayout = (LinearLayout) toast.getView();
                    TextView toastTV = (TextView) toastLayout.getChildAt(0);
                    toastTV.setTextSize(30);
                    toast.show();
                }
            }
        });
    }

    private AlertDialog.Builder alertDialogInit(String title, String hint, int inputType){
        AlertDialog.Builder ad = new AlertDialog.Builder(getActivity());
        ad.setTitle(title);
        input = new EditText(getActivity());
        input.setInputType(inputType);
        input.setTransformationMethod(PasswordTransformationMethod.getInstance());
        input.setHint(hint);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        ad.setView(input);
        ad.setPositiveButton("OK",
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        //Do nothing here because we override this button later to change the close behaviour.
                        //However, we still need this because on older versions of Android unless we
                        //pass a handler the button doesn't get instantiated
                    }
                });
        ad.setCancelable(true);
        return ad;
    }


    public Bitmap rotate(Bitmap src, float degree) {
        Matrix matrix = new Matrix();
        matrix.setRotate(degree);
        Bitmap bitmap = Bitmap.createBitmap(src, 0, 0, src.getWidth(),
                src.getHeight(), matrix, true);
        return bitmap;
    }

    public String getSavedPinCode() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String shareText = prefs.getString(Const.PINCODE, "22563");
        return shareText;
    }

    public void refreshList(){
        getActivity().getSupportLoaderManager().getLoader(0).forceLoad();
    }

    public void update(){

        JSONObject jsonObject=null;
        ByteArrayEntity entity = null;

        try {
            jsonObject = new JSONObject();
            jsonObject.put("registrationid", ((MainActivity) getActivity()).getRegid());
            JSONArray jsonArray = new JSONArray();
            JSONObject queryObj = new JSONObject();
            queryObj.put("type", "medialist");

            jsonArray.put(queryObj);
            jsonObject.put("query", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("jsonObject", jsonObject.toString());
        try {
            entity = new ByteArrayEntity(jsonObject.toString().getBytes("UTF-8"));
        }
        catch(UnsupportedEncodingException e){
            e.printStackTrace();

        }

        client.post(getActivity(), getResources().getString(R.string.api_url), entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, String.valueOf(response));
                try {
                    JSONArray array = response.getJSONArray("response");
                    //String status = array.getJSONObject(0).getString("status");
                    JSONArray medialist = array.getJSONObject(0).getJSONArray("medialist");
                    db.delAllRec();
                    for (int i=0;i<medialist.length();i++) {
                        Object jsonMediaObject = medialist.get(i);
                        String serverId = ((JSONObject)jsonMediaObject).getString("id");
                        String std = ((JSONObject)jsonMediaObject).getString("std");
                        String mid = ((JSONObject)jsonMediaObject).getString("mid");
                        String low = ((JSONObject)jsonMediaObject).getString("low");
                        String thumb = ((JSONObject)jsonMediaObject).getString("thumb");
                        Log.e("id", serverId);
                        Log.e("std", std);
                        Log.e("mid", mid);
                        Log.e("low", low);
                        Log.e("thumb", thumb);
                        db.addRec(serverId, low, mid, std, thumb);
                    }
                    Toast toast =Toast.makeText(getActivity(), R.string.updated, Toast.LENGTH_LONG);
                    LinearLayout toastLayout = (LinearLayout) toast.getView();
                    TextView toastTV = (TextView) toastLayout.getChildAt(0);
                    toastTV.setTextSize(30);
                    toast.show();
                    refreshList();
                    //String type = array.getJSONObject(0).getString("type");
                    /*Log.d(TAG, status);
                    // Log.d(TAG, type);

                    if (status.equals("true")) {

                    }*/

                } catch (JSONException e) {
                    Log.w(TAG, "Parsing error", e);
                }
            }

            @Override
            public void onFailure(Throwable e, JSONObject errorResponse) {
                super.onFailure(e, errorResponse);
            }
        });
    }


    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (cursor!=null) {
            cursor.close();
        }
    }




    static class MyCursorLoader extends CursorLoader {

        DB db;

        public MyCursorLoader(Context context, DB db) {
            super(context);
            this.db = db;
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getAllData();
            return cursor;
        }
    }


    private class GridViewImageLoadListener implements ImageLoadingListener{
        long position= -1;

        private GridViewImageLoadListener(long position) {
            this.position = position;
        }

        @Override
        public void onLoadingStarted(String s, View view) {

        }

        @Override
        public void onLoadingFailed(String s, View view, FailReason failReason) {

        }

        @Override
        public void onLoadingComplete(String s, View view, Bitmap loadedBitmap) {
            //Bitmap resizedBitmap;
            if (loadedBitmap.getWidth() < loadedBitmap.getHeight()) {
                loadedBitmap = rotate(loadedBitmap, -90);
            }
           /* else{
                loadedBitmap = loadedBitmap;
            }*/
            ((ImageView)view).setImageBitmap(loadedBitmap);
        }

        @Override
        public void onLoadingCancelled(String s, View view) {

        }
    }


    private class GridViewBinder implements SimpleCursorAdapter.ViewBinder{


        private class ViewHolder{
            ImageView imageView;
            int position;
        }

        @Override
        public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
            if (columnIndex == cursor.getColumnIndex(DB.FIELD_IMAGE_LINK_THUMB)) {
                ViewHolder holder = new ViewHolder();

                String linkThumb= cursor.getString(columnIndex);
                holder.imageView = (ImageView) view.findViewById(R.id.iv_photo);
                Long id = cursor.getLong(cursor.getColumnIndex("_id"));
                imageLoader.displayImage(linkThumb, holder.imageView, options, new GridViewImageLoadListener(id));
                view.setTag(holder);

                return true;
            }
            return false;
        }
    }
}
