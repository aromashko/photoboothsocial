package pbs.regionit.ua.photoboothsocial.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.commonsware.cwac.camera.CameraFragment;
import com.commonsware.cwac.camera.CameraHost;
import com.commonsware.cwac.camera.CameraView;

/**
 * Created by Lotar on 08.02.2015.
 */
public class SelfieFragment extends CameraFragment {
    private CameraView cameraView=null;
    private CameraHost host=null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        cameraView=new CameraView(getActivity());
        cameraView.setHost(getHost());

        return(cameraView);
    }
}
