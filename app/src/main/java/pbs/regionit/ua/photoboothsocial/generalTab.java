package pbs.regionit.ua.photoboothsocial;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import pbs.regionit.ua.photoboothsocial.utils.GcmUtils;
import pbs.regionit.ua.photoboothsocial.utils.Const;

/**
 * Created by RmIIIK on 11.11.2014.
 */
public class generalTab extends Fragment implements View.OnClickListener {

    String TAG = generalTab.class.getSimpleName();

    GoogleCloudMessaging gcm;

    String regid;
    private static final int REQUEST_CODE_GALLERY_BACKGROUND =1;
    private static final int REQUEST_CODE_GALLERY_LOGO =2;
    Button btn_connect_terminal, btn_change_background, btn_change_to_default, btn_change_pincode;
    Boolean successfullRegisterGCM=false;
    EditText input;

    /** Called when the activity is first created. */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.other_tab, container, false);
        initView(v);


        return v;
    }

    public void initView(View v){
        btn_connect_terminal = (Button) v.findViewById(R.id.btn_connect_terminal);
        btn_change_background = (Button) v.findViewById(R.id.btn_change_background);
        //btn_change_logo= (Button) v.findViewById(R.id.btn_change_logo);
        btn_change_to_default= (Button) v.findViewById(R.id.btn_change_to_default);
        //btn_hide_logo= (Button) v.findViewById(R.id.btn_hide_logo);
        btn_change_pincode= (Button) v.findViewById(R.id.btn_change_pincode);
        btn_connect_terminal.setOnClickListener(this);
        btn_change_background.setOnClickListener(this);
        //btn_change_logo.setOnClickListener(this);
        btn_change_to_default.setOnClickListener(this);
        btn_change_pincode.setOnClickListener(this);
        //btn_hide_logo.setOnClickListener(this);
    }

    private void pickImage(int requestCode){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_connect_terminal:
                IntentIntegrator.forSupportFragment(generalTab.this).initiateScan();
                break;
            case R.id.btn_change_background:
                pickImage(REQUEST_CODE_GALLERY_BACKGROUND);
                break;
            /*case R.id.btn_change_logo:
                pickImage(REQUEST_CODE_GALLERY_LOGO);
                break;*/
            case R.id.btn_change_to_default:
                storePrefs(Const.SAVED_BACKGROUND, "");
                break;
            case R.id.btn_change_pincode:
                showPinAlertDialog();
                break;
            /*case R.id.btn_hide_logo:
                break;*/
        }

    }

    public void showPinAlertDialog(){
        final AlertDialog.Builder ad = alertDialogInit(getString(R.string.pin_alert_title), getString(R.string.new_pin_alert_hint), InputType.TYPE_CLASS_NUMBER);
        ad.setPositiveButton("Ок", new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                storePrefs(Const.PINCODE, input.getText().toString());
            }
        });
        ad.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.cancel();
            }
        });
        final AlertDialog dialog = ad.create();
        dialog.show();

    }

    private AlertDialog.Builder alertDialogInit(String title, String hint, int inputType){
        AlertDialog.Builder ad = new AlertDialog.Builder(getActivity());
        ad.setTitle(title);
        input = new EditText(getActivity());
        input.setInputType(inputType);
        input.setTransformationMethod(PasswordTransformationMethod.getInstance());
        input.setHint(hint);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        ad.setView(input);
        ad.setCancelable(true);
        return ad;
    }

    private void registerInBackground() {
        new RegisterGCMTask().execute(null, null, null);

    }

    class RegisterGCMTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            Toast toast =Toast.makeText(getActivity(), R.string.try_connect_to_google, Toast.LENGTH_LONG);
            LinearLayout toastLayout = (LinearLayout) toast.getView();
            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(30);
            toast.show();
            //Toast.makeText(getActivity(), "Please wail a minute. We trying connect to google service.", Toast.LENGTH_LONG).show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            String msg = "";
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(getActivity());
                }
                regid = gcm.register(((SettingsActivity) getActivity()).SENDER_ID);
                msg = "Device registered, registration ID=" + regid;

                // You should send the registration ID to your server over HTTP,
                // so it can use GCM/HTTP or CCS to send messages to your app.
                // The request to your server should be authenticated if your app
                // is using accounts.
                //sendRegistrationIdToBackend();

                // For this demo: we don't need to send it because the device
                // will send upstream messages to a server that echo back the
                // message using the 'from' address in the message.

                // Persist the regID - no need to register again.
               GcmUtils.storeRegistrationId(getActivity(), regid);
                successfullRegisterGCM = true;
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
                // If there is an error, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off.
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String msg) {
            if (successfullRegisterGCM) {
                Toast.makeText(getActivity(), "Connect successfull. Please try connect to terminal again", Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(getActivity(), "Cannot connect to google service", Toast.LENGTH_LONG).show();
            }
            Log.e("Registration ID", msg);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {
            // handle scan result
            registerInBackEnd(scanResult);
        }
        else if(requestCode == REQUEST_CODE_GALLERY_BACKGROUND && resultCode == Activity.RESULT_OK && data!= null){
            Uri selectedImageUri = data.getData();
            String selectedImagePath = getPath(selectedImageUri);
            storePrefs(Const.SAVED_BACKGROUND, selectedImagePath);
        }
        else if(requestCode == REQUEST_CODE_GALLERY_LOGO && resultCode == Activity.RESULT_OK && data!= null){
            Uri selectedImageUri = data.getData();
            String selectedImagePath = getPath(selectedImageUri);
            storePrefs(Const.SAVED_LOGO, selectedImagePath);
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * helper to retrieve the path of an image URI
     */
    public String getPath(Uri uri) {
        if( uri == null ) {
            return null;
        }
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    public void storePrefs(String key, String value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void storePrefs(String key,boolean value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void registerInBackEnd(IntentResult scanResult){
        AsyncHttpClient client = new AsyncHttpClient();
        String contents = scanResult.getContents();
        JSONObject jsonObject=null;
        ByteArrayEntity entity = null;
        if (contents != null) {
            String regId= ((SettingsActivity)getActivity()).getRegid();
            Log.e("General Tab", regId + " regId");
            if (regId.isEmpty()){
                registerInBackground();
            }
            else{
                try {
                   jsonObject = new JSONObject();
                    jsonObject.put("registrationid", regId);
                    JSONArray jsonArray = new JSONArray();
                    JSONObject queryObj = new JSONObject();
                    queryObj.put("type",  "auth");
                    queryObj.put("authcode", contents);
                    jsonArray.put(queryObj);
                    jsonObject.put("query", jsonArray);
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
                Log.e("jsonObject",jsonObject.toString());
                try {
                    entity = new ByteArrayEntity(jsonObject.toString().getBytes("UTF-8"));
                }
                catch(UnsupportedEncodingException e){
                    e.printStackTrace();

                }


                client.post(getActivity(), getResources().getString(R.string.api_url), entity, "application/json",  new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(JSONObject response) {
                        Log.d(TAG, String.valueOf(response));
                        try {
                            JSONArray array = response.getJSONArray("response");
                            String status = array.getJSONObject(0).getString("status");
                            String type = array.getJSONObject(0).getString("type");
                            Log.d(TAG, status);
                            Log.d(TAG, type);

                            if (status.equals("true") && type.equals("auth")){
                                storePrefs(Const.CONNECTED_TO_TERMINAL, true);
                            }

                        } catch (JSONException e) {
                            Log.w(TAG, "Parsing error", e);
                        }
                    }

                    @Override
                    public void onFailure(Throwable e, JSONObject errorResponse) {
                        super.onFailure(e, errorResponse);
                    }
                });

            }
        }
    }


}