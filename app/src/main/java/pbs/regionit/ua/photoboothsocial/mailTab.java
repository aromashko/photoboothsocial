package pbs.regionit.ua.photoboothsocial;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import pbs.regionit.ua.photoboothsocial.utils.Const;

/**
 * Created by RmIIIK on 11.11.2014.
 */
public class mailTab extends Fragment implements CompoundButton.OnCheckedChangeListener {

    Switch swch_activate;
    EditText edt_share_text;
    CheckBox chb_save_login;

    /** Called when the activity is first created. */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mail_tab, container, false);
        initView(v);

        edt_share_text.setText(getShareTextPrefs());
        edt_share_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                storePrefs(Const.MAIL_SHARE_TEXT, s.toString());
            }
        });

        return v;
    }

    public void initView(View v){
        swch_activate = (Switch) v.findViewById(R.id.swch_activate);
        edt_share_text = (EditText) v.findViewById(R.id.edt_share_text);
        chb_save_login= (CheckBox) v.findViewById(R.id.chb_save_login);
        swch_activate.setChecked(getActivatedPrefs());

        swch_activate.setOnCheckedChangeListener(this);
        chb_save_login.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.swch_activate:
                storePrefs(Const.MAIL_ACTIVATED, isChecked);
                break;
            case R.id.chb_save_login:
                break;
        }
    }

    public boolean getActivatedPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.MAIL_ACTIVATED, false);
        return autoShare;
    }

    public String getShareTextPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String shareText = prefs.getString(Const.MAIL_SHARE_TEXT, "");
        return shareText;
    }

    public void storePrefs(String key,boolean value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void storePrefs(String key, String value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }
}