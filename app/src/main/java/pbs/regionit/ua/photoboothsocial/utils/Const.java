package pbs.regionit.ua.photoboothsocial.utils;

import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.github.gorbin.asne.instagram.InstagramSocialNetwork;
import com.github.gorbin.asne.vk.VkSocialNetwork;

/**
 * Created by user on 08.12.2014.
 */
public class Const {

    /* Prefs */
    public static final String PREFS = "prefs";
    public static final String SAVED_BACKGROUND="saved_background";
    public static final String SAVED_LOGO="saved_logo";
    public static final String CONNECTED_TO_TERMINAL="connected_to_terminal";

    public static final String FB_ACTIVATED="fb_activated";
    public static final String FB_SAVE_LOGIN="fb_save_login";
    public static final String FB_SHARE_TEXT="fb_share_text";

    public static final String VK_ACTIVATED="vk_activated";
    public static final String VK_SAVE_LOGIN="vk_save_login";
    public static final String VK_SHARE_TEXT="vk_share_text";
    public static final String VK_ALBUM_NAME="vk_album_name";

    public static final String SMS_ACTIVATED="sms_activated";
    public static final String SMS_SHARE_TEXT="sms_share_text";
    public static final String SMS_PHONENUMBER_PREFYKS="sms_phonenumber_prefyks";

    public static final String MAIL_ACTIVATED="mail_activated";
    public static final String MAIL_SHARE_TEXT="mail_share_text";

    public static final String PRINT_ACTIVATED="print_activated";

    public static final String INSTAGRAM_ACTIVATED="instagram_activated";
    public static final String INSTAGRAM_PRINT_ACTIVATED="instagram_print_activated";
    public static final String INSTAGRAM_PRINT_FIXED_TAG_ACTIVATED="instagram_print_fixed_tag_activated";
    public static final String INSTAGRAM_LIMIT_PHOTO_PRINT="instagram_limit_photo_print";
    public static final String INSTAGRAM_SHARE_TEXT="instagram_share_text";
    public static final String INSTAGRAM_FIXED_HASHTAG="instagram_fixed_hashtag";
    public static final String INSTAGRAM_COUNT_OF_PHOTO_TO_VIEW="instagram_count_of_photo_to_view";
    public static final String INSTAGRAM_MAX_COUNT_PHOTO_TO_PRINT="INSTAGRAM_MAX_COUNT_PHOTO_TO_PRINT";
    public static final String INSTAGRAM_PERIOD_TO_PRINT="instagram_period_to_print";

    public static final String SELFIE_ACTIVATED="selfie_activated";
    public static final String SHOW_SELFIE_IN_PHOTOBOOTH_ACTIVATED="show_selfie_in_photobooth";

    public static final String SESSION_ACTIVATED="session_activated";


    public static final String NEED_LOGOUT="need_logout";
    public static final String PINCODE="pincode";


    /**
     * SocialNetwork Ids in ASNE:
     * 1 - Twitter
     * 2 - LinkedIn
     * 3 - Google Plus
     * 4 - Facebook
     * 5 - Vkontakte
     * 6 - Odnoklassniki
     * 7 - Instagram
     */

    public static final int VKONTAKTE = VkSocialNetwork.ID;
    public static final int FACEBOOK = FacebookSocialNetwork.ID;
    public static final int INSTAGRAM = InstagramSocialNetwork.ID;
    public static final String SOCIAL_NETWORK_TAG_VK = "SocialIntegrationMain.SOCIAL_NETWORK_TAG_VK";
    public static final String SOCIAL_NETWORK_TAG_FACEBOOK = "SocialIntegrationMain.SOCIAL_NETWORK_TAG_FACEBOOK";
    public static final String SOCIAL_NETWORK_TAG = "SocialIntegrationMain.SOCIAL_NETWORK_TAG";
}
