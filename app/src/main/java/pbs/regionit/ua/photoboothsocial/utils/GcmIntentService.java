package pbs.regionit.ua.photoboothsocial.utils;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import pbs.regionit.ua.photoboothsocial.MainActivity;
import pbs.regionit.ua.photoboothsocial.R;
import pbs.regionit.ua.photoboothsocial.db.DB;

/**
 * Created by Lotar on 02.12.2014.
 */
public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    String TAG = GcmIntentService.class.getSimpleName();
    DB db;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        Log.i(TAG, "Received");
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " +
                        extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                String type = extras.getString("type","");
                if (type.equals("session")){
                    if (db == null) {
                        db = new DB(getApplicationContext());
                    }
                    db.open();
                    db.delAllRec();
                    Intent intent_refresh_list = new Intent(MainActivity.BROADCAST_ACTION);
                    sendBroadcast(intent_refresh_list);
                    Intent intent_log_out = new Intent(MainActivity.BROADCAST_ACTION_LOG_OUT);
                    sendBroadcast(intent_log_out);
                    storePrefs(Const.NEED_LOGOUT, true);
                }
                else {
                    String id = extras.getString("id", "");
                    String std = extras.getString("std","");
                    String mid = extras.getString("mid","");
                    String low = extras.getString("low", "");
                    String thumb = extras.getString("thumb","");
                    Log.e("id", id);
                    Log.e("std", std);
                    Log.e("mid", mid);
                    Log.e("low", low);
                    Log.e("thumb", thumb);
                    if (db == null) {
                        db = new DB(getApplicationContext());
                    }
                    db.open();
                    db.addRec(id, low, mid, std, thumb);
                    Intent intent_refresh_list = new Intent(MainActivity.BROADCAST_ACTION);
                    sendBroadcast(intent_refresh_list);
                }
                // Post notification of received message.
                //sendNotification("Received: " + extras.toString());
                Log.i(TAG, "Received: " + extras.toString());
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    public void storePrefs(String key,boolean value) {
        final SharedPreferences prefs = getSharedPreferences(Const.PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }




    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("GCMIntentService", "onDestroy");
        db.close();
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_fb)
                        .setContentTitle("GCM Notification")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
