
package pbs.regionit.ua.photoboothsocial.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Handler;

public class InfinitiveDrawable extends Drawable {

    private static final int UPDATE_DELAY = 40;
    private static final float PIXELS_PER_UPDATE = 4f;
    private Bitmap mBitmap;
    private Paint mPaint;
    private Handler mHandler = new Handler();

    Runnable mMoveRunnable = new Runnable() {

        @Override
        public void run() {
            mY = mY + PIXELS_PER_UPDATE;
            invalidateSelf();
        }
    };
    private float mY = 0f;

    public InfinitiveDrawable(Bitmap bitmap, int width) {
        if (bitmap.getWidth() == width) {
            mBitmap = bitmap;
        } else {
            mBitmap = Bitmap.createScaledBitmap(bitmap, width, width * bitmap.getHeight() / bitmap.getWidth(), false);
            bitmap.recycle();
        }

        mPaint = new Paint();
    }

    @Override
    public void draw(Canvas canvas) {

        float yPos = mY % mBitmap.getHeight();
        canvas.drawBitmap(mBitmap, 0, yPos, mPaint);

        canvas.drawBitmap(mBitmap, 0, yPos - mBitmap.getHeight(), mPaint);

        mHandler.postDelayed(mMoveRunnable, UPDATE_DELAY);

    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {

    }

    @Override
    public int getOpacity() {
        return mPaint.getAlpha();
    }

}
