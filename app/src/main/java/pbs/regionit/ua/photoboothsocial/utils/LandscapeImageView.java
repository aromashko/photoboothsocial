package pbs.regionit.ua.photoboothsocial.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by user on 21.01.2015.
 */
public class LandscapeImageView extends ImageView {
    public LandscapeImageView(Context context) {
        super(context);
    }

    public LandscapeImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LandscapeImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


}
