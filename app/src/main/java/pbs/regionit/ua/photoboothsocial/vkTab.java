package pbs.regionit.ua.photoboothsocial;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.SocialNetworkManager;
import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.github.gorbin.asne.vk.VkSocialNetwork;
import com.vk.sdk.VKScope;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pbs.regionit.ua.photoboothsocial.db.DB;
import pbs.regionit.ua.photoboothsocial.utils.Const;

/**
 * Created by RmIIIK on 11.11.2014.
 */
public class vkTab extends android.support.v4.app.Fragment implements CompoundButton.OnCheckedChangeListener, SocialNetworkManager.OnInitializationCompleteListener, OnLoginCompleteListener {

    Switch swch_activate;
    EditText et_fb_login, et_fb_password, edt_share_text, edt_vk_album_name;
    Button btn_share;
    CheckBox chb_save_login;
    public SocialNetwork socialNetwork;
    public static SocialNetworkManager mSocialNetworkManager;
    private int networkId = Const.VKONTAKTE;
    String TAG = vkTab.class.getSimpleName();
    public String VK_KEY;
    DB db;




    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.vkontakte_tab, container, false);
        initView(v);

        VK_KEY = getString(R.string.vk_app_id);
        edt_share_text.setText(getShareTextPrefs());
        edt_share_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                storePrefs(Const.VK_SHARE_TEXT, s.toString());
            }
        });
        edt_vk_album_name.setText(getAlbumNamePrefs());
        edt_vk_album_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                storePrefs(Const.VK_ALBUM_NAME, s.toString());
            }
        });

        String[] vkScope = new String[] {
                VKScope.WALL,
                VKScope.PHOTOS,
                VKScope.NOHTTPS,
        };
        ArrayList<String> fbScope = new ArrayList<String>();
        fbScope.addAll(Arrays.asList("public_profile, email, user_friends"));
        mSocialNetworkManager = (SocialNetworkManager) getFragmentManager().findFragmentByTag(Const.SOCIAL_NETWORK_TAG);

        //Check if manager exist
        if (mSocialNetworkManager == null) {
            mSocialNetworkManager = new SocialNetworkManager();

            VkSocialNetwork vkNetwork = new VkSocialNetwork(this, VK_KEY, vkScope);
            mSocialNetworkManager.addSocialNetwork(vkNetwork);

            FacebookSocialNetwork fbNetwork = new FacebookSocialNetwork(vkTab.this, fbScope);
            mSocialNetworkManager.addSocialNetwork(fbNetwork);

            //Initiate every network from mSocialNetworkManager
            getFragmentManager().beginTransaction().add(mSocialNetworkManager, Const.SOCIAL_NETWORK_TAG).commit();
            mSocialNetworkManager.setOnInitializationCompleteListener(this);
        } else {
            //if manager exist - get and setup login only for initialized SocialNetworks
            if(!mSocialNetworkManager.getInitializedSocialNetworks().isEmpty()) {
                List<SocialNetwork> socialNetworks = mSocialNetworkManager.getInitializedSocialNetworks();
                for (SocialNetwork socialNetwork : socialNetworks) {

                    socialNetwork.setOnLoginCompleteListener(this);
                }
            }
        }
        db = ((SettingsActivity) getActivity()).getDb();
        return v;
    }

    public void initView(View v){
        swch_activate = (Switch) v.findViewById(R.id.swch_activate);
        btn_share = (Button) v.findViewById(R.id.btn_share);
        edt_share_text = (EditText) v.findViewById(R.id.edt_share_text);
        edt_vk_album_name = (EditText) v.findViewById(R.id.edt_vk_album_name);
        chb_save_login= (CheckBox) v.findViewById(R.id.chb_save_login);
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                socialNetwork = mSocialNetworkManager.getSocialNetwork(networkId);
                if (socialNetwork.isConnected()) {
                    socialNetwork.logout();
                }
                loginMaker();
            }
        });

        swch_activate.setChecked(getActivatedPrefs());
        chb_save_login.setChecked(getSaveLoginPrefs());
        swch_activate.setOnCheckedChangeListener(this);
        chb_save_login.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.swch_activate:
                storePrefs(Const.VK_ACTIVATED, isChecked);
                break;
            case R.id.chb_save_login:
                storePrefs(Const.VK_SAVE_LOGIN, isChecked);
                break;
        }
    }

    public void loginMaker() {
        socialNetwork = mSocialNetworkManager.getSocialNetwork(networkId);
        if(!socialNetwork.isConnected()) {
            ((SettingsActivity) getActivity()).showProgress("Loading social person");
            Log.e(TAG, "loginMaker");
            socialNetwork.requestLogin();
        }
        else{
            sharing();
        }
    }

    public void sharing() {
        Log.e(TAG, "Sharing");
        Cursor c = null;
        ArrayList<String> links = new ArrayList<String>();
        c = db.getNotSharedPictureVk();
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    db.updateImageSharedVk();
                    links.add(c.getString(c.getColumnIndex(DB.FIELD_IMAGE_LINK_MID)));
                    Log.e(TAG, c.getString(c.getColumnIndex(DB.FIELD_IMAGE_LINK_MID)));
                } while (c.moveToNext());
            }
            c.close();
        } else{
            Log.d(TAG, "Cursor is null");
        }
        if (links.size()==0){
            Toast toast =Toast.makeText(getActivity(), R.string.nothing_to_share, Toast.LENGTH_LONG);
            LinearLayout toastLayout = (LinearLayout) toast.getView();
            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(30);
            toast.show();
        }else {
            try {
                ((VkSocialNetwork) socialNetwork).makeSharing(links);
            }
            catch (NullPointerException e){
                e.printStackTrace();
                Toast toast =Toast.makeText(getActivity(), R.string.something_wrong, Toast.LENGTH_LONG);
                LinearLayout toastLayout = (LinearLayout) toast.getView();
                TextView toastTV = (TextView) toastLayout.getChildAt(0);
                toastTV.setTextSize(30);
                toast.show();
            }
        }
    }


    public boolean getActivatedPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.VK_ACTIVATED, false);
        return autoShare;
    }

    public boolean getSaveLoginPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        boolean autoShare = prefs.getBoolean(Const.VK_SAVE_LOGIN, false);
        return autoShare;
    }

    public String getShareTextPrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String shareText = prefs.getString(Const.VK_SHARE_TEXT, "");
        return shareText;
    }

    public String getAlbumNamePrefs() {
        final SharedPreferences prefs =  getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        String albumName = prefs.getString(Const.VK_ALBUM_NAME, "");
        return albumName;
    }

    public void storePrefs(String key,boolean value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void storePrefs(String key, String value) {
        final SharedPreferences prefs = getActivity().getSharedPreferences(Const.PREFS, getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


    }

    @Override
    public void onSocialNetworkManagerInitialized() {
        Log.e(TAG, "SocialNetworkManagerInitialized");
        //socialNetwork.
        for (SocialNetwork socialNetwork : mSocialNetworkManager.getInitializedSocialNetworks()) {
            Log.e(TAG, "onSocialNetworkManagerInitialized setOnLoginCompleteListener");
            socialNetwork.setOnLoginCompleteListener(this);
        }
    }

    @Override
    public void onLoginSuccess(int i) {
        Log.e(TAG, "LoginSuccess");
        sharing();
        ((SettingsActivity) getActivity()).hideProgress();
    }

    @Override
    public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {
        Log.e(TAG, "socialNetworkID= " + socialNetworkID + ",requestID= " + requestID + ",errorMessage= " + errorMessage + ",data= " + data);
         ((SettingsActivity) getActivity()).hideProgress();
    }

}